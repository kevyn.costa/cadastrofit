<?php
class Calculo {
    private $conn;
    private $table_name = "calculadora";

    public $aluno_id;

    public function __construct($db) {
        $this->conn = $db;
    }

    public function read() {
        $query = "SELECT * FROM " . $this->table_name . " WHERE id_aluno = ?";
        $stmt = $this->conn->prepare($query);
        $stmt->execute([$this->aluno_id]);
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
}
