<?php

class UserRegistration {
    private $db;
    private $message;

    public function __construct(Database $db) {
        $this->db = $db->getConnection();
        session_start();
    }

    public function registerUser($nome, $email, $senha, $isAdmin) {
        $verificarEmail = $this->db->prepare("SELECT * FROM usuarios WHERE email = :email");
        $verificarEmail->bindParam(':email', $email);
        $verificarEmail->execute();

        if ($verificarEmail->rowCount() > 0) {
            $this->message = 'Erro: O e-mail já está em uso. Escolha outro e-mail.';
            return false;
        } else {
            $inserirUsuario = $this->db->prepare("INSERT INTO usuarios (nome, email, senha, isadmin) VALUES (:nome, :email, :senha, :isadmin)");
            $inserirUsuario->bindParam(':nome', $nome);
            $inserirUsuario->bindParam(':email', $email);
            $senhaHash = password_hash($senha, PASSWORD_DEFAULT);
            $inserirUsuario->bindParam(':senha', $senhaHash);
            $inserirUsuario->bindParam(':isadmin', $isAdmin, PDO::PARAM_BOOL);

            if ($inserirUsuario->execute()) {
                $this->message = 'Novo usuário cadastrado com sucesso!';
                header("Location: ../dashboards/dashboard_usuarios.php?id={$_SESSION['usuario_id']}");
                exit;
            } else {
                $this->message = 'Erro ao cadastrar novo usuário. Tente novamente.';
                return false;
            }
        }
    }

    public function getMessage() {
        return $this->message;
    }
}

?>
