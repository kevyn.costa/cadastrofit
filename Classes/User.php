<?php
class User {
    private $db;
    private $userId;
    private $isadmin;

    public function __construct(Database $db) {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        
        $this->db = $db->getConnection();

        if (isset($_SESSION['usuario_id'])) {
            $this->userId = $_SESSION['usuario_id'];
            $this->isadmin = isset($_SESSION['isadmin']) ? $_SESSION['isadmin'] : false;
        }
    }

    public function isLogged() {
        return isset($_SESSION['usuario_id']);
    }

    public function getUserData() {
        if ($this->isLogged()) {
            $stmt = $this->db->prepare("SELECT * FROM usuarios WHERE id = :usuario_id");
            $stmt->bindParam(':usuario_id', $_SESSION['usuario_id']);
            $stmt->execute();
            return $stmt->fetch();
        }
        return null;
    }

    public function isAdmin() {
        return $this->isadmin;
    }

    public function login($email, $senha) {
        try {
            $stmt = $this->db->prepare("SELECT * FROM usuarios WHERE email = :email");
            $stmt->bindParam(':email', $email);
            $stmt->execute();

            $usuario = $stmt->fetch();

            if ($usuario && password_verify($senha, $usuario['senha'])) {
                $_SESSION['usuario_id'] = $usuario['id'];
                $_SESSION['isadmin'] = $usuario['isadmin'];
                $_SESSION['nome_admin'] = $usuario['nome'];

                header('Location: dashboards/dashboard_alunos.php');
                exit;
            } else {
                $_SESSION['login_error'] = "E-mail ou senha inválidos.";
                header('Location: index.php');
                exit;
            }
        } catch (Exception $e) {
            throw new Exception("Erro na execução da consulta: " . $e->getMessage());
        }
    }

    public function logout() {
        session_unset();
        session_destroy();
        header('Location: index.php');
        exit;
    }

    public function getAllUsers() {
        $stmt = $this->db->query("SELECT * FROM usuarios");
        return $stmt->fetchAll();
    }

    public function changePassword($id, $nova_senha, $confirma_nova_senha) {
        if ($nova_senha === $confirma_nova_senha) {
            $hashed_nova_senha = password_hash($nova_senha, PASSWORD_DEFAULT);
            $stmt = $this->db->prepare("UPDATE usuarios SET senha = :nova_senha WHERE id = :id");
            $stmt->bindParam(':nova_senha', $hashed_nova_senha);
            $stmt->bindParam(':id', $id);
            return $stmt->execute();
        } else {
            throw new Exception("A nova senha e a confirmação não coincidem.");
        }
    }

    public function getUserDetails($id) {
        $stmt = $this->db->prepare("SELECT * FROM usuarios WHERE id = :usuario_id");
        $stmt->bindParam(':usuario_id', $id);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    public function getCurrentUserId() {
        return $this->userId;
}

    public function getUserByEmail($email) {
        $query = $this->db->prepare("SELECT * FROM usuarios WHERE email = :email");
        $query->bindParam(':email', $email);
        $query->execute();
        return $query->fetch(PDO::FETCH_ASSOC);
}

    public function setTokenAndExpiration($email, $token, $expiration) {
        $query = $this->db->prepare("UPDATE usuarios SET token = :token, data_expiracao = :expiracao WHERE email = :email");
        $query->bindParam(':token', $token);
        $query->bindParam(':expiracao', $expiration);
        $query->bindParam(':email', $email);
        $query->execute();
    }

    public function validateToken($email, $token) {
        $query = $this->db->prepare("SELECT * FROM usuarios WHERE email = :email AND token = :token AND data_expiracao > NOW()");
        $query->bindParam(':email', $email);
        $query->bindParam(':token', $token);
        $query->execute();
        return $query->fetch(PDO::FETCH_ASSOC);
    }

    public function updatePassword($email, $nova_senha) {
        $query = $this->db->prepare("UPDATE usuarios SET senha = :senha, token = NULL, data_expiracao = NULL WHERE email = :email");
        $query->bindParam(':senha', password_hash($nova_senha, PASSWORD_DEFAULT));
        $query->bindParam(':email', $email);
        return $query->execute();
    }
}
?>