<?php

class Formulas {
    public function calcularDensidadeCorporalMasculina($antropometria, $idade) {
        $constante = 1.109380;
        $coeficiente1 = -0.0008267;
        $coeficiente2 = 0.0000016;
        $coeficiente3 = -0.0002574;

        $densidade_corporal = $constante +
                              $coeficiente1 * ($antropometria['peitoral'] + $antropometria['abdomem'] + $antropometria['coxa_medial']) +
                              $coeficiente2 * pow(($antropometria['peitoral'] + $antropometria['abdomem'] + $antropometria['coxa_medial']), 2) +
                              $coeficiente3 * $idade;

        return $densidade_corporal;
    }

    public function calcularDensidadeCorporalFeminina($antropometria, $idade) {
        $constante = 1.0994921;
        $coeficiente1 = -0.0009929;
        $coeficiente2 = 0.0000023;
        $coeficiente3 = -0.0001392;

        $densidade_corporal = $constante +
                              $coeficiente1 * ($antropometria['triceps'] + $antropometria['supra_iliaca_medial'] + $antropometria['coxa_medial']) +
                              $coeficiente2 * pow(($antropometria['triceps'] + $antropometria['supra_iliaca_medial'] + $antropometria['coxa_medial']), 2) +
                              $coeficiente3 * $idade;

        return $densidade_corporal;
    }

    public function calcularPercentualGorduraCorporal($densidade_corporal) {
        $constante = 4.95;
        $subtrativo = 4.50;

        $percentual_gordura_corporal = (($constante / $densidade_corporal) - $subtrativo) * 100;

        return $percentual_gordura_corporal;
    }

    public function calcularPercentualGorduraMeninos($antropometria) {
        $percentual_gordura = (0.735 * ($antropometria['triceps'] + $antropometria['perna']) + 1);
        return $percentual_gordura;
    }

    public function calcularPercentualGorduraMeninas($antropometria) {
        $percentual_gordura = (0.610 * ($antropometria['triceps'] + $antropometria['perna']) + 5.1);
        return $percentual_gordura;
    }

    public function calcularMassaMagra($antropometria, $percentual_gordura_corporal){
        if (!isset($antropometria['peso']) || empty($antropometria['peso'])) {
            return 'Peso não disponível. Não é possível calcular a massa magra.';
        }

        $massa_magra = $antropometria['peso'] * $percentual_gordura_corporal/100;
        return $massa_magra;

    }
}
?>
