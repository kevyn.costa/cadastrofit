<?php
session_start();
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include '../Classes/Database.php';
include '../Classes/Aluno.php';
include '../Classes/AlunoRepository.php';

$db = new Database();
$alunoRepo = new AlunoRepository($db);
$alunoClass = new Aluno($db);

$id_aluno = isset($_GET['id']) ? $_GET['id'] : null;
$data_escolhida = isset($_GET['data_escolhida']) ? $_GET['data_escolhida'] : null;

if (!$id_aluno) {
    echo "ID do aluno inválido.";
    exit;
}

$aluno = $alunoClass->getAlunoDetails($id_aluno);
$datasAnamnese = $alunoRepo->getDatasAnamnese($id_aluno);
$datasAntropometria = $alunoRepo->getDatasAntropometria($id_aluno);

$anamnese = $alunoRepo->getAnamnese($id_aluno, $data_escolhida);
$antropometria = $alunoRepo->getAntropometria($id_aluno, $data_escolhida);
//$testesFisicos = $alunoRepo->getTestesFisicos($id_aluno, $data_escolhida);
$objetivos = $alunoRepo->getObjetivos($id_aluno);

$idade = $alunoClass->getIdade($aluno['data_nascimento']);

$_SESSION['aluno_id'] = $id_aluno;
$_SESSION['data_escolhida'] = $data_escolhida;
$_SESSION['objetivos'] = $objetivos;

$_SESSION['aluno'] = $aluno;
?>

<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Detalhes do Aluno</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css">
    <link rel="stylesheet" type="text/css" href="../css/dashboard_detalhes.css">
    <link rel="stylesheet" href="../css/style_calc.css">
    <link rel="shortcut icon" href="../img/icon.png" type="image/x-icon">

</head>

<body>

    <div class="container">

        <div class="content">
            <h2>Detalhes do Aluno</h2>

            <table class="student-table">
                <tr>
                    <td class="student-info">
                        <strong>Nome:</strong> <?= ucwords($aluno['nome']) ?>
                    </td>
                </tr>
                <tr>
                    <td class="student-info">
                        <strong>Telefone:</strong> <?= $aluno['telefone'] ?>
                    </td>
                </tr>
                <tr>
                    <td class="student-info">
                        <strong>Sexo:</strong> <?= $aluno['sexo'] ?>
                    </td>
                </tr>
                <tr>
                    <td class="student-info">
                        <strong>Data de Nascimento:</strong> <?= date('d/m/Y', strtotime($aluno['data_nascimento'])) ?>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <!-- Adicione quantos espaços você achar adequado -->
                        <strong>Idade:</strong> <?= $idade ?> anos
                    </td>
                </tr>

                <?php if (!empty($data_escolhida)) : ?>
                   
                    <!-- Adiciona mais detalhes -->
                <?php endif; ?>
            </table>

            <div class="navBar">
                <div class="cont">
                    <h4>Exames :</h4>
                    <li class="smooth-hover"><a href="../forms/novaAnamneseForm.php?id=<?= $aluno['id'] ?>"><i class="fa-regular fa-clipboard"></i>Anamnese</a></li>
                    <li class="smooth-hover"><a href="../forms/novaAntropometriaForm.php?id=<?= $aluno['id'] ?>"><i class="fa-regular fa-clipboard"></i>Antropometria</a></li>
                    <li class="smooth-hover"><a href="../forms/novoTesteFisicoForm.php?id=<?= $aluno['id'] ?>"><i class="fa-regular fa-clipboard"></i>Testes Físicos</a></li>
                </div>
                <div class="cont">
                    <h4>Relatorios :</h4>
                    <li class="smooth-hover" id="gerarRelatorioBtnRelatorio" style="z-index: 2; position: relative;">
                    <a href="javascript:void(0);" onclick="toggleListaDatas('listaDatasRelatorio', 'setaIconRelatorio');" style="display: flex; align-items: center;">
                    <i class="fas fa-file-alt"></i> Relatório Parcial
                    <i id="setaIconRelatorio" class="fas fa-chevron-down" style="margin-left: 5px;"></i>
                    </a>
                    <ul id="listaDatasRelatorio" style="display:none; text-align: left; padding: 0; margin: 0; z-index: 1; list-style: none;">
                        <?php
                            foreach ($datasAntropometria as $data) {
                            $data_formatada = date('d/m/Y', strtotime($data));
                            echo "<li style='margin: 5px; margin-right: 20px;'><a href=\"../templates/relatorioDate.php?id=" . $aluno['id'] . "&data_escolhida=" . urlencode($data) . "\">" . $data_formatada . "</a></li>";
                            }
                        ?>
                    </ul>
                </li>
                    <li class="smooth-hover"><a href="../templates/relatorio.php?id=<?= $aluno['id'] ?>"><i class="fas fa-file-alt"></i> Relatório Completo</a>

                </div>
                <div class="cont">
                    <h4>Outros :</h4>
                    <li class="smooth-hover" id="Calculadora" style="z-index: 2; position: relative;">

                        <a href="javascript:void(0);" id="calc" style="display: flex; align-items: center;">
                            <i class="fa-solid fa-calculator"></i> Calculadora
                        </a>
                            <?php
                            //foreach ($datas as $data) {
                            //    $data_formatada = date('d/m/Y', strtotime($data));
                                //echo "<li style='margin: 5px; margin-right: 20px;'><a href=\"../dashboards/calculadora.php?id=" . $aluno['id'] . "&data_escolhida=" . $data . "\">" . $data_formatada . "</a></li>";
                            //}
                            ?>
                    </li>
                    <li class="smooth-hover"><a href="../views/excluirAluno.php?id=<?= $aluno['id']; ?>" onclick="return confirmarExclusao(event, <?= $aluno['id']; ?>)"><i class="fas fa-trash-alt" style="color: red;"></i>Excluir aluno</a></li>
                    <li class="smooth-hover"><a href="../dashboards/dashboard_alunos.php"><i class="fas fa-arrow-left"></i> Voltar</a>
                </div>
            </div>

        </div>

        <div id="calculadora" class="hiden">
            <div class="data_form">
            <select name="Data" id="selectData">
                    <option value="">Data</option>
                    <?php
                    foreach ($datasAntropometria as $data) {
                        $data_formatada = date('d/m/Y', strtotime($data));
                        echo "<option value=\"$data\">$data_formatada</option>";
                    }
                    ?>
                </select>

                <select name="formula" id="selectFormula">
                    <option value="">Formulas</option>
                        <?php if (strtolower($aluno['sexo']) === "masculino") : ?>
                        <option value="percentual_gordura_masculina">Percentual de Gordura Corporal Masculina</option>
                        <option value="percentual_gordura_meninos">Percentual de Gordura Corporal Meninos (8-18 anos)</option>
                        <option value="massa_magra">Massa Magra</option>
                    <?php elseif (strtolower($aluno['sexo']) === "feminino") : ?>
                        <option value="percentual_gordura_feminina">Percentual de Gordura Corporal Feminina</option>
                        <option value="percentual_gordura_meninas">Percentual de Gordura Corporal Meninas (8-18 anos)</option>
                        <option value="massa_magra">Massa Magra</option>
                    <?php else : ?>
                        <option value="percentual_gordura_masculina">Percentual de Gordura Corporal Masculina</option>
                        <option value="percentual_gordura_feminina">Percentual de Gordura Corporal Feminina</option>
                        <option value="percentual_gordura_meninos">Percentual de Gordura Corporal Meninos (8-18 anos)</option>
                        <option value="percentual_gordura_meninas">Percentual de Gordura Corporal Meninas (8-18 anos)</option>
                        <option value="massa_magra">Massa Magra</option>
                    <?php endif; ?>
                </select>

            </div>

            <?php
                // Adiciona o campo oculto no HTML
                echo "<input type='hidden' id='idadeAluno' value='$idade'>";
            ?>
            <h4 id="resultado">Resultados :</h4>

            <div class="btn">
                <button id="salvarDadosBtn">Salvar</button>
            </div>
        </div>

    </div>

    <script>
document.getElementById('selectFormula').addEventListener('change', function() {
    var selectedFormula = this.value;
    var idadeAluno = parseInt(document.getElementById('idadeAluno').value);
    var idAluno = <?php echo json_encode($id_aluno); ?>;
    var selectedData = document.getElementById('selectData').value;

    if (isFormulaInvalidForAge(selectedFormula, idadeAluno)) {
        alert("É impossível calcular usando esta fórmula pois o aluno tem entre 8 e 18 anos de idade.");
        this.value = ''; // Reseta o valor do campo de seleção
        return; // Impede a execução do cálculo
    }

    if (selectedFormula && selectedData) {
        makeAjaxRequest(idAluno, selectedFormula, selectedData);
    } else {
        alert("Por favor, selecione uma data e uma fórmula.");
    }
});

function isFormulaInvalidForAge(formula, age) {
    return (formula === 'percentual_gordura_masculina' || formula === 'percentual_gordura_feminina') && age >= 8 && age <= 18;
}

function makeAjaxRequest(idAluno, selectedFormula, selectedData) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', 'calcular.php?id=' + encodeURIComponent(idAluno) + '&formula=' + encodeURIComponent(selectedFormula) + '&data=' + encodeURIComponent(selectedData), true);
    xhr.onload = function() {
        if (xhr.status === 200) {
            processAjaxResponse(xhr.responseText, selectedFormula);
        } else {
            alert('Erro ao calcular o percentual de gordura corporal.');
        }
    };
    xhr.send();
}

function processAjaxResponse(responseText, selectedFormula) {
    try {
        var resposta = JSON.parse(responseText); // Converte a resposta JSON para objeto
        
        if (resposta.error) {
            document.getElementById('resultado').innerText = "Erro: " + resposta.error;
        } else {
            var resultado = resposta[selectedFormula];
            if (resultado !== undefined) {
                displayResult(selectedFormula, resultado);
            } else {
                alert('Resultado inválido recebido.');
            }
        }
    } catch (e) {
        alert('Erro ao processar a resposta: ' + e.message);
    }
}

function displayResult(formula, resultado) {
    var formulaDisplayName = formatFormulaName(formula);
    var unidade = formula.includes('percentual_gordura') ? '%' : 'kg';
    document.getElementById('resultado').innerText = "Resultado: " + formulaDisplayName + ": " + resultado + unidade;
}

function formatFormulaName(formula) {
    return formula.replace(/_/g, ' ').replace(/\b\w/g, function(match) {
        return match.toUpperCase();
    });
}

</script>

    </script>

<script>
document.getElementById('salvarDadosBtn').addEventListener('click', function() {
    var selectedFormula = document.getElementById('selectFormula').value;
    var selectedData = document.getElementById('selectData').value;
    var idAluno = <?php echo json_encode($_SESSION['aluno_id']); ?>;
    var idadeAluno = <?php echo json_encode($idade); ?>;

    console.log('selectedFormula:', selectedFormula);
    console.log('selectedData:', selectedData);
    console.log('idAluno:', idAluno);
    console.log('idadeAluno:', idadeAluno);

    if (selectedFormula && selectedData && idAluno && idadeAluno) {
        var xhr = new XMLHttpRequest();
        xhr.open('POST', 'processarDados.php', true);
        xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        xhr.onload = function() {
            if (xhr.status === 200) {
                console.log(xhr.responseText);
                var response = JSON.parse(xhr.responseText);
                if (response.success) {
                    alert(response.success);
                } else {
                    alert('Erro: ' + response.error);
                }
            } else {
                alert('Erro ao salvar os dados.');
            }
        };
        var postData = 'id_aluno=' + encodeURIComponent(idAluno) + 
                       '&data_escolhida=' + encodeURIComponent(selectedData) + 
                       '&formula_escolhida=' + encodeURIComponent(selectedFormula) + 
                       '&idade_aluno=' + encodeURIComponent(idadeAluno);
        console.log('postData:', postData); // Log the postData for debugging
        xhr.send(postData);
    } else {
        alert("Por favor, selecione uma data, uma fórmula, e certifique-se de que os dados do aluno estão corretos.");
    }
});
</script>

    <script>
        function confirmarExclusao(event, alunoId) {
            var resposta = confirm("Tem certeza que deseja excluir este aluno?");
            if (!resposta) {
                window.location.replace("../public/detalhes_aluno.php?id=<?= $aluno['id']; ?>");
                return false;
            }
            return true;
        }

        // Impede a propagação do evento de clique da tr para os elementos filhos
        document.querySelectorAll('.delete-container').forEach(function(container) {
            container.addEventListener('click', function(event) {
                event.stopPropagation();
            });
        });
    </script>

    <!-- Script para escolher data do relatório -->
    <script>
        function toggleListaDatas(listaId, iconeId) {
            var listaDatas = document.getElementById(listaId);
            var setaIcon = document.getElementById(iconeId);

            if (listaDatas.style.display === 'none') {
                listaDatas.style.display = 'block';
                setaIcon.classList.remove('fa-chevron-down');
                setaIcon.classList.add('fa-chevron-up');
            } else {
                listaDatas.style.display = 'none';
                setaIcon.classList.remove('fa-chevron-up');
                setaIcon.classList.add('fa-chevron-down');
            }
        }
    </script>
    <!-- modal calc -->
    <script>
        let calc = document.querySelector("#calc")

        calc.addEventListener("click", () => {
            document.querySelector("#calculadora").classList.toggle("calculadora")
            document.querySelector("#calculadora").classList.toggle("hiden")
        })
    </script>

</body>
</html>