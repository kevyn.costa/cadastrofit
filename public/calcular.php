<?php
require_once '../Classes/AlunoRepository.php';
require_once '../Classes/Aluno.php';
require_once '../Classes/Formulas.php';
require_once '../Classes/Database.php';

header('Content-Type: application/json');

class CalcularController {
    private $alunoRepository;
    private $aluno;
    private $formulas;

    public function __construct() {
        $database = new Database();
        $this->alunoRepository = new AlunoRepository($database);
        $this->aluno = new Aluno($database);
        $this->formulas = new Formulas();
    }

    public function processarCalculo($id_aluno, $formula, $data_escolhida) {
        if (!$id_aluno || !$formula || !$data_escolhida) {
            return json_encode(["error" => "Dados de entrada inválidos."]);
        }

        $antropometria = $this->alunoRepository->getAntropometria($id_aluno, $data_escolhida);

        if (!$antropometria) {
            return json_encode(["error" => "Nenhuma antropometria encontrada para o aluno e data especificados."]);
        }

        $dados_aluno = $this->aluno->getAlunoDetails($id_aluno);
        $data_nascimento = $dados_aluno['data_nascimento'];
        $sexo = $dados_aluno['sexo'];

        if (!$data_nascimento || !$sexo) {
            return json_encode(["error" => "Dados do aluno incompletos."]);
        }

        $idade = $this->aluno->getIdade($data_nascimento);
        $resultado = null;

        switch ($formula) {
            case 'percentual_gordura_masculina':
                $resultado = $this->calcularPercentualGorduraMasculina($idade, $antropometria);
                break;
            case 'percentual_gordura_feminina':
                $resultado = $this->calcularPercentualGorduraFeminina($idade, $antropometria);
                break;
            case 'percentual_gordura_meninos':
                $resultado = $this->formulas->calcularPercentualGorduraMeninos($antropometria);
                break;
            case 'percentual_gordura_meninas':
                $resultado = $this->formulas->calcularPercentualGorduraMeninas($antropometria);
                break;
            case 'massa_magra':
                $resultado = $this->calcularMassaMagra($idade, $antropometria, $sexo);
                break;
            default:
                return json_encode(["error" => "Fórmula desconhecida."]);
        }

        if (is_string($resultado)) {
            return json_encode(["error" => $resultado]);
        }

        // Formata o resultado e retorna no formato JSON
        return json_encode([$formula => number_format($resultado, 2)]);
    }

    private function calcularPercentualGorduraMasculina($idade, $antropometria) {
        if ($idade >= 8 && $idade <= 18) {
            return $this->formulas->calcularPercentualGorduraMeninos($antropometria);
        } else {
            $densidade_corporal = $this->formulas->calcularDensidadeCorporalMasculina($antropometria, $idade);
            if (is_numeric($densidade_corporal)) {
                return $this->formulas->calcularPercentualGorduraCorporal($densidade_corporal);
            } else {
                return $densidade_corporal; // Erro retornado pela função de densidade corporal
            }
        }
    }

    private function calcularPercentualGorduraFeminina($idade, $antropometria) {
        if ($idade >= 8 && $idade <= 18) {
            return $this->formulas->calcularPercentualGorduraMeninas($antropometria);
        } else {
            $densidade_corporal = $this->formulas->calcularDensidadeCorporalFeminina($antropometria, $idade);
            if (is_numeric($densidade_corporal)) {
                return $this->formulas->calcularPercentualGorduraCorporal($densidade_corporal);
            } else {
                return $densidade_corporal; // Erro retornado pela função de densidade corporal
            }
        }
    }

    private function calcularMassaMagra($idade, $antropometria, $sexo) {
        if ($idade >= 8 && $idade <= 18) {
            if ($sexo == 'masculino') {
                $percentual_gordura_corporal = $this->formulas->calcularPercentualGorduraMeninos($antropometria);
            } else {
                $percentual_gordura_corporal = $this->formulas->calcularPercentualGorduraMeninas($antropometria);
            }
        } else {
            if ($sexo == 'masculino') {
                $densidade_corporal = $this->formulas->calcularDensidadeCorporalMasculina($antropometria, $idade);
            } else {
                $densidade_corporal = $this->formulas->calcularDensidadeCorporalFeminina($antropometria, $idade);
            }
    
            if (is_numeric($densidade_corporal)) {
                $percentual_gordura_corporal = $this->formulas->calcularPercentualGorduraCorporal($densidade_corporal);
            } else {
                return 'Erro ao calcular percentual de gordura corporal.';
            }
        }
    
        $massa_magra = $this->formulas->calcularMassaMagra($antropometria, $percentual_gordura_corporal);
        if (is_string($massa_magra)) {
            return $massa_magra; // Retorna mensagem de erro
        }
        return $massa_magra;
    }
}

// Processamento da requisição
$id_aluno = isset($_GET['id']) ? $_GET['id'] : null;
$formula = isset($_GET['formula']) ? $_GET['formula'] : null;
$data_escolhida = isset($_GET['data']) ? $_GET['data'] : null;

$controller = new CalcularController();
echo $controller->processarCalculo($id_aluno, $formula, $data_escolhida);
?>
